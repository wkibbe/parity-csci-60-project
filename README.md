# Parity Program

This program sorts and maintains even and odd numbers from a given array. The code provided here contains the implementation of the `Parity` class, which includes member functions for inserting, removing, and accessing elements as well as memory management functions.

Please note that the header file `parity.h` and any driver files are not included in this repository, as the author of this implementation is not the author of those files.

# Usage

To use this implementation, you need to have the parity.h header file and a driver file to utilize the functions in this class. Once you have these files, you can compile and run the program as required.

# Features

- Constructor that takes an array and its size as parameters.
- Copy constructor for memory management.
- Destructor to delete dynamically allocated memory.
- Assignment operator overload for memory management.
- Functions for inserting, removing, and accessing elements.
- Overloaded << operator for output.