#include "parity.h"

  Parity::Parity(int * a, size_t n) //2 parameter constructor
  {
    //define the pointers and set their size equal to CAP
    evens_ = new int [CAP];
    odds_ = new int [CAP];
    nEven_=0;
    nOdd_=0;

    //fill the new object from the array
    for(int i = 0; i<n; i++)
    {
      insert(a[i]);
    }
  }

  // NEW memory management functions
  Parity::Parity(const Parity& rhs) //copy constructor
  {
    //define the pointers and set their size equal to CAP
    evens_ = new int [CAP];
    odds_ = new int [CAP];
    nEven_=0;
    nOdd_=0;
    //fill the new object from the array
    for(int i = 0; i<rhs.nOdd_; i++)
    {
     insert(rhs.odds_[i]);
    } 
    for(int i = 0; i<rhs.nEven_; i++)
    {
     insert(rhs.evens_[i]); 
    }
  }
  

  Parity::~Parity() //destructor member function
  {
    //get rid of whatever is currently in the object without orphaning memory on the heap
    nEven_=0;
    nOdd_=0;
    delete [] odds_;
    delete [] evens_;
  }

  Parity& Parity::operator =(const Parity& rhs) //assignment operator overload
  //for this, let's just delete the whole lhs object and refill it with the right
  {
    //deleting all of the current data
    nEven_=0;
    nOdd_=0;
    delete [] odds_;
    delete [] evens_;

    //define the pointers and set their size equal to CAP
    evens_ = new int [CAP];
    odds_ = new int [CAP];
    nEven_=0;
    nOdd_=0;
    //fill the new object from the array
    for(int i = 0; i<rhs.nOdd_; i++)
    {
     insert(rhs.odds_[i]);
    } 
    for(int i = 0; i<rhs.nEven_; i++)
    {
     insert(rhs.evens_[i]); 
    }

    return *this;
  }

  // NEW and revised mutators (must meet specifications from assignment HO)
  void Parity::insert(int val) // adds a single copy of val, maintaining order
  {
    int bigindex = 0;
    if(val%2==0)
    {
      //even insertion code
      if(nEven_%CAP==0) //deal with heap reassignment issues
      {
        //copies the heap array for processing
        int temporaryarray [nEven_]; //creates temporary stack array to deep copy
        int temporaryarraysize = nEven_;
        for(int i = 0; i < nEven_; i++) // copies the old heap array to the stack array
        {
          temporaryarray[i] = evens_[i];
        }
        //deletes the old heap array
        delete [] evens_;
        //reassigns the pointer to a new array of the correct size
        evens_ = new int [nEven_+CAP];
        //copies the information into the new heap array
        for(int i = 0; i < nEven_; i++)
        {
          evens_[i] = temporaryarray[i];
        }
      }
      //after ensuring the heap array is the right size, we now must ensure proper sorting
      //now, let's find the index in the array where val belongs
      bigindex = nEven_;
      for(int i = 0; i < nEven_; i++) //find the first value in the array that is bigger than val
      {
        if(evens_[i]>val)
        {
          bigindex = i;
          break;
        }
      }

      //now lets shift the array to make room in the right spot
      if(bigindex<nEven_)
      {
        for(int i = nEven_-1; i >= bigindex; i--)
        {
          evens_[i+1] = evens_[i];
        }
      }
      //now lets insert val to replace the duplicate value at bigindex
      evens_[bigindex]=val;
      nEven_++;
    }
    
    if(val%2!=0)
    {
      //odd insertion code
      if(nOdd_%CAP==0)
      {
        //deal with heap reassignment issues first
        //copies the heap array for processing
        int temporaryarray [nOdd_]; //creates temporary stack array to deep copy
        int temporaryarraysize = nOdd_;
        for(int i = 0; i < nOdd_; i++) // copies the old heap array to the stack array
        {
          temporaryarray[i] = odds_[i];
        }
        //deletes the old heap array
        delete [] odds_;
        //reassigns the pointer to a new array of the correct size
        odds_ = new int [nOdd_+CAP];
        //copies the information into the new heap array
        for(int i = 0; i < nOdd_; i++)
        {
          odds_[i] = temporaryarray[i];
        }
      }
      //after ensuring the heap array is the right size, we now must ensure proper sorting
      //now, let's find the index in the array where val belongs
      int bigindex = nOdd_;
      for(int i = 0; i < nOdd_; i++) //find the first value in the array that is bigger than val
      {
        if(odds_[i]>val)
        {
          bigindex = i;
          break;
        }
      }

      //now lets shift the array to make room in the right spot
      if(bigindex<nOdd_)
      {
        for(int i = nOdd_-1; i >= bigindex; i--)
        {
          odds_[i+1] = odds_[i];
        }
      }
      
      //now lets insert val to replace the duplicate value at bigindex
      odds_[bigindex]=val;
      nOdd_++;

    }
  }
  size_t Parity::remove(int val) // removes ALL copies of val and returns the # of times val was removed
  {
    int numremoved = 0;
    int temporaryarray[nOdd_+nEven_];
    int temporaryarraysize = nOdd_+nEven_;
    //fill in the temporary stack array
    for(int i=0; i<nOdd_;i++)
    {
      temporaryarray[i]=odds_[i];
    }
    for(int i=nOdd_; i<temporaryarraysize;i++)
    {
      temporaryarray[i]=evens_[i-nOdd_];
    }
    //now lets delete/reset the heap arrays and reset the counts
    delete [] evens_;
    delete [] odds_;
    evens_ = new int [CAP];
    odds_ = new int [CAP];
    nOdd_=0;
    nEven_=0;

    //now lets fill up the new array while filtering out val
    for(int i = 0; i < temporaryarraysize; i++)
    {
      if(temporaryarray[i]!=val)
      {
        insert(temporaryarray[i]);
      }
      if(temporaryarray[i]==val)
      {
        numremoved++;
      }
    }
    return numremoved;
  }

  // accessors
  int Parity::min() const
  {
    int small = odds_[0];
    for(int i = 0; i<nEven_;i++)
    {
      if(evens_[i]<small)
      {
        small = evens_[i];
      }
    }
    for(int i = 0; i<nOdd_;i++)
    {
      if(odds_[i]<small)
      {
        small = odds_[i];
      }
    }
    return small;
  }
  int Parity::max() const
  {
    int large = odds_[0];
    for(int i = 0; i<nEven_;i++)
    {
      if(evens_[i]>large)
      {
        large = evens_[i];
      }
    }
    for(int i = 0; i<nOdd_;i++)
    {
      if(odds_[i]>large)
      {
        large = odds_[i];
      }
    }
    return large;
  }
  bool Parity::odd() const
  {
    int sum = 0;
    for(int i = 0; i < nEven_; i++)
    {
      sum += evens_[i];
    }
    for(int i = 0; i < nOdd_; i++)
    {
      sum+= odds_[i];
    }
    return((sum%2)!=0);
  }

  ostream& operator <<(ostream& out, const Parity& p) //out stream operator overload friend function
  {
    out<< "even: ";
    for(int i = 0; i<p.nEven_; i++)
    {
      out << p.evens_[i]<<", ";
    }
    out<<endl;
    out << "odd: ";
    for(int i = 0; i<p.nOdd_; i++)
    {
      out << p.odds_[i]<<", ";
    }
    out<<endl;
    return out;
  }